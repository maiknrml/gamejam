extends KinematicBody2D

const MOVE_SPEED = 300

const FIREBALL = preload("res://Scenes/fireball.tscn")
const MAGICBULLET = preload("res://Scenes/MagicBullet.tscn")
const ENEMY = preload("res://Scenes/Enemy.tscn")

enum Weapon 	{Fist, Sword, Bow, Handgun, Rifle, MagicRod}
enum Direction 	{idle, up, right, down, left, upRight, upLeft, downRight, downLeft} 

var walkDirection 	= Direction.idle
var Equipped_Weapon = Weapon.MagicRod

var looksLeft = false
var isIdle = true;
var i = 0
var count = 0
var fik = Vector2(400.0,400.0)
var m_magicCounter = 6

var look_vec = get_global_mouse_position() - global_position


func _ready():
	yield(get_tree(), "idle_frame")
	get_tree().call_group("zombies", "set_player", self)


func _translateDirection(p_DirVec = Vector2()):
	isIdle = false
	
	if p_DirVec.x == 0 && p_DirVec.y == 0:	_moveIdle()
	if p_DirVec.x == 0 && p_DirVec.y == 1:	_moveUp()
	if p_DirVec.x == 1 && p_DirVec.y == 1:	_moveUpRight()	
	if p_DirVec.x == 1 && p_DirVec.y == 0:	_moveRight()
	if p_DirVec.x == 1 && p_DirVec.y == -1:	_moveDownRight()
	if p_DirVec.x == 0 && p_DirVec.y == -1:	_moveDown()	
	if p_DirVec.x == -1 && p_DirVec.y == -1:_moveDownLeft()
	if p_DirVec.x == -1 && p_DirVec.y == 0:	_moveLeft()
	if p_DirVec.x == 1 && p_DirVec.y == -1:	_moveUpLeft()


func _moveIdle():
	isIdle = true
	walkDirection = Direction.idle		#ToDo fertig machen
	if   (looksLeft == false):
		$AnimatedSprite.play("idle_right")
	else:
		$AnimatedSprite.play("idle_left")
		#$AnimatedSprite.play("walk_up")

func _moveLeft():
	looksLeft = true
	print(position.x)
	walkDirection = Direction.left
	$AnimatedSprite.play("walk_left")

func _moveRight():
	looksLeft = false
	print(position.x)
	walkDirection = Direction.right
	$AnimatedSprite.play("walk_right")

func _moveUp():
	walkDirection = Direction.up
	_diagonalMove();

func _moveDown():
	walkDirection = Direction.down
	_diagonalMove();

func _moveUpRight():
	walkDirection = Direction.upRight
	_diagonalMove();

func _moveDownRight():
	walkDirection = Direction.downRight
	_diagonalMove();

func _moveUpLeft():
	walkDirection = Direction.upLeft
	_diagonalMove();

func _moveDownLeft():
	walkDirection = Direction.downLeft
	_diagonalMove();

func _diagonalMove():
	if   (looksLeft == false):
		$AnimatedSprite.play("walk_right")
	else:
		$AnimatedSprite.play("walk_left")
		#$AnimatedSprite.play("walk_up")
	
func _handleMove(p_Delta):
	var move_vec = Vector2()
	isIdle = true;

	if Input.is_action_pressed("move_left"):
		if position.x > 10:
			move_vec.x -= 1

	elif Input.is_action_pressed("move_right"):
		if position.x < 4300:
			move_vec.x += 1

	if Input.is_action_pressed("move_up"):
		if position.y > 10:
			move_vec.y -= 1

	elif Input.is_action_pressed("move_down"):
		if position.y <4300:
			move_vec.y += 1
	
	_translateDirection(move_vec)

	move_vec = move_vec.normalized()
	move_and_collide(move_vec * MOVE_SPEED * p_Delta)


func _attackShoot(p_look_vec = Vector2()):
	var fireball = FIREBALL.instance()
	var normalV = Vector2()
	var winkel  = 0
	normalV.x = 0
	normalV.y = 1
	
	get_parent().add_child(fireball)

	fireball.position = ($Position2D.global_position + (fireball.m_Abstand * p_look_vec))
	fireball.velocity = p_look_vec.normalized() * fireball.SPEED
	
	if(p_look_vec.x > 0):
		winkel = (p_look_vec.normalized().dot(normalV) * 90)
	else:
		winkel = 180 - (p_look_vec.normalized().dot(normalV) * 90)

	fireball.rotate(deg2rad(winkel))
	
	
func _attackMagic(p_look_vec = Vector2()):
	var fireball = MAGICBULLET.instance()
	var normalV = Vector2()
	var winkel  = 0
	var wildMagic = 0
	normalV.x = 0
	normalV.y = 1
	
	get_parent().add_child(fireball)

	fireball.position = ($Position2D.global_position + (fireball.m_Abstand * p_look_vec))
	fireball.velocity = p_look_vec.normalized() * fireball.SPEED
	
	if(p_look_vec.x > 0):
		winkel = (p_look_vec.normalized().dot(normalV) * 90)
	else:
		winkel = 180 - (p_look_vec.normalized().dot(normalV) * 90)

	
	fireball.sprite.frame = m_magicCounter
		
	var scaleO = Vector2()
	scaleO.x = 5
	scaleO.y = 5
	match m_magicCounter:
		6:
			 fireball.transform.scaled(scaleO)
		7:
			i
		8:
			i
		9:
			i
		10:
			i
	
	
func _handleAttack(p_Delta):
	look_vec = get_global_mouse_position() - global_position
	look_vec = look_vec.normalized()
	#global_rotation = atan2(look_vec.y, look_vec.x)

	if Input.is_action_just_pressed("shoot"):
		match Equipped_Weapon:
			Weapon.Fist:
				i
			Weapon.Sword:
				i
			Weapon.Bow:
				_attackShoot(look_vec)
			Weapon.Handgun:
				i
			Weapon.Rifle:
				i
			Weapon.MagicRod:
				_attackMagic(look_vec)
	
	if Input.is_action_just_pressed("Weapon 1"):
		m_magicCounter = 6
	if Input.is_action_just_pressed("Weapon 2"):
		m_magicCounter = 7
	if Input.is_action_just_pressed("Weapon 3"):
		m_magicCounter = 8
	if Input.is_action_just_pressed("Weapon 4"):
		m_magicCounter = 9
	if Input.is_action_just_pressed("Weapon 5"):
		m_magicCounter = 10
	if Input.is_action_just_pressed("Huenchen"):
		m_magicCounter = 4
		_huenchenPower(look_vec)

func _huenchenPower(p_look_vec = Vector2()):
	var Power = 9001
	if Power > 9000:
		var fireball = MAGICBULLET.instance()
		var normalV = Vector2()
		var winkel  = 0
		normalV.x = 0
		normalV.y = 1
		
		get_parent().add_child(fireball)
	
		fireball.position = ($Position2D.global_position + (fireball.m_Abstand * p_look_vec))
		fireball.velocity = p_look_vec.normalized() * fireball.SPEED
		
		if(p_look_vec.x > 0):
			winkel = (p_look_vec.normalized().dot(normalV) * 90)
		else:
			winkel = 180 - (p_look_vec.normalized().dot(normalV) * 90)

		fireball.rotate(deg2rad(winkel))
		fireball.velocity = fireball.velocity / 9001
	
func _physics_process(delta):
	var direction = Vector2()
	var velocity = Vector2()
	
	_handleMove(delta)
	_handleAttack(delta)

	if Input.is_action_just_pressed("Pause"):
		get_tree().change_scene("res://Scenes/RotateEarths.tscn")
	
	count += 1
	if count == 100:
		
	#if Input.is_action_just_pressed("spawn"):
		var enemy = ENEMY.instance()

		get_parent().add_child(enemy)
		var rand = randi()%4
		if rand == 0:
			fik = Vector2(400.0,400.0)
		if rand == 1:
			fik = Vector2(-400.0,400.0)
		if rand == 2:
			fik = Vector2(400.0,-400.0)
		if rand == 3:
			fik = Vector2(-400.0,-400.0)

		enemy.position = $Position2D.global_position + fik

		count = 0

func kill():
	get_tree().reload_current_scene()
	
	
