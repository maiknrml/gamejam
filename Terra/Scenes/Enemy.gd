extends KinematicBody2D

var SPEED = 70
onready var obj = get_parent().get_node("Player")

var velocity = Vector2()

var is_dead = false


func dead():
	is_dead = true
	velocity = Vector2(0, 0)
	$AnimatedSprite.play("dead")
	$CollisionShape2D.call_deferred("set_disabled", true)
	 
func _physics_process(delta):
	if is_dead == false:
		var dir = (obj.global_position - global_position).normalized()
		move_and_collide(dir * SPEED * delta)
		$AnimatedSprite.play("walk")
		

