extends CanvasLayer

var kills = 100
var amountOfKills = 0
var levelName = "The Forest"
var booster = 0



func _ready():
	get_node("KillCounter").text="Kills: " + str(amountOfKills) + "/ " + str(kills)
	get_node("LevelName").text="Current Level: " + str(levelName) 
	get_node("Booster").hide()
	pass
	
func _process(delta):
	if booster == 4:
		get_node("Booster").show()
	pass
